/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#define APP_VERSION "1.0.3"

#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>
#include <iostream>
#include <camera/CameraMetadata.h>
#include <qmmf-sdk/qmmf_recorder.h>
#include <qmmf-sdk/qmmf_recorder_extra_param_tags.h>
#include "qmmf_rtsp_server_interface.h"
#include "qmmf_mux_interface.h"

using namespace qmmf;
using namespace recorder;
using namespace muxinterface;

#define DEFAULT_RTSP_PORT 8900
#define DEFAULT_JPEG_QUALITY 80

const int NO_ERRORS = 0;
const int ERROR = -1;


int camera_number = 0; // Camera to use (0-2)

std::string video_output_format = "h264";

Recorder my_recorder;
uint32_t session_id=0;
AVQueue* rtsp_video_queue;

MuxInterface* mux = NULL;
bool save_video = false;  //create mp4 file?
bool running    = true;

int control_fifo_fd = -1;
int output_fifo_fd = -1;
int write_to_stdout = 0;
int dump_nth_frame  = 0;

//stats
uint32_t frame_cntr              = 0;
int64_t last_frame_timestamp_us  = 0;
int64_t first_frame_timestamp_us = 0;
uint64_t track_bytes_total       = 0;
float   average_fps              = 0.0;


int create_control_fifo()
{
    const char * camera_control_fifo_fmt = "/dev/camera%d_control";
    char camera_control_fifo[256];
    int len = snprintf(camera_control_fifo,sizeof(camera_control_fifo),camera_control_fifo_fmt,camera_number);
    printf("Camera Control FIFO starting: %s\n",camera_control_fifo);
    mkfifo(camera_control_fifo, 0666);
    control_fifo_fd = open(camera_control_fifo, O_RDONLY | O_NONBLOCK);
}

int create_output_fifo(std::string fifo_name)
{
    //const char * camera_output_fifo_fmt = "/dev/camera%d_output";
    //char camera_output_fifo_name[256];
    //int len = snprintf(camera_output_fifo_name,sizeof(camera_output_fifo_name),
    //                   camera_output_fifo_fmt,camera_number);

    printf("Camera Output FIFO starting: %s .. will block until client is ready\n",fifo_name.c_str());
    mkfifo(fifo_name.c_str(), 0666);
    output_fifo_fd = open(fifo_name.c_str(), O_WRONLY); // | O_NONBLOCK
    if (output_fifo_fd > 0)
        printf("Created output fifo, fd: %d\n",output_fifo_fd);
    else
        printf("Failed to open output fifo..\n");

    return output_fifo_fd;
}


int64_t get_time_us()
{
  struct timespec t;
  clock_gettime( CLOCK_MONOTONIC, &t );
  uint64_t timeMicroSecMonotonic = (uint64_t)(t.tv_sec) * 1000000ULL + t.tv_nsec/1000;
  return (int64_t)timeMicroSecMonotonic;
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
void signal_handler(int dummy) {
    running = false;
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
void RecorderEventCb(EventType event_type, void *event_data,
                     size_t event_data_size) {
    if (event_type == EventType::kCameraError) {
        printf("Camera error recorder event\n");
    }
    else if (event_type == EventType::kServerDied) {
        printf("Server died recorder event\n");
    }
    else {
        printf("Unknown recorder event\n");
    }
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
void SessionEventCb(EventType event_type, void *event_data,
                    size_t event_data_size) {
    // TBD: Once support for this callback is present in QMMF
    printf("Got a Session callback\n");
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
void VideoTrackDataCb(uint32_t session,
                      uint32_t track,
                      std::vector<BufferDescriptor> buffers,
                      std::vector<MetaData> meta_data) {

    int32_t buffer_index = -1;  //buffer index for accessing array of meta info

    for (auto& iter : buffers) {
        buffer_index++;
        frame_cntr++;
        int64_t timestamp_us = iter.timestamp;

        //BUG: timestamp for h264/h265 is in microseconds, otherwise nanoseconds
        if ((video_output_format!="h264") && (video_output_format!="h265"))
            timestamp_us/= 1000;


        if (video_output_format=="yuv"){
            //remove the gap between Y and UV planes and empty space after UV planes
            //note: this assumes width = stride !!!
            //note: if format selected to yuv-padded, this block will not run, using the original padded buffer

            int y_plane_full_size =  meta_data[buffer_index].cam_buffer_meta_data.plane_info[0].stride *
                                     meta_data[buffer_index].cam_buffer_meta_data.plane_info[0].scanline;

            int y_plane_real_size =  meta_data[buffer_index].cam_buffer_meta_data.plane_info[0].stride *
                                     meta_data[buffer_index].cam_buffer_meta_data.plane_info[0].height;

            int uv_plane_real_size = meta_data[buffer_index].cam_buffer_meta_data.plane_info[1].stride *
                                     meta_data[buffer_index].cam_buffer_meta_data.plane_info[1].height;


            uint8_t * uv_plane_full_start = ((uint8_t*)iter.data) + y_plane_full_size;
            uint8_t * uv_plane_new_start  = ((uint8_t*)iter.data) + y_plane_real_size;

            //only move memory if necessary
            if (uv_plane_full_start != uv_plane_new_start){
                memmove(uv_plane_new_start, uv_plane_full_start, uv_plane_real_size);
            }

            //update the size since the allocated size could still be larger than actual size
            iter.size = y_plane_real_size + uv_plane_real_size;
        }

        track_bytes_total+= iter.size;

        if (write_to_stdout)
        {
            fwrite((uint8_t *)iter.data,iter.size,1,stdout);
            fflush(stdout);
        }

        if (output_fifo_fd>0)
        {
            write(output_fifo_fd,(uint8_t *)iter.data,iter.size);
            fsync(output_fifo_fd);
        }

        if ((dump_nth_frame>0) && ((frame_cntr%dump_nth_frame)==0))
        {
            const char * file_out_fmt = "camera%d_frame%06d_%lld.%s";
            char file_out_name[256];
            int len = snprintf(file_out_name,sizeof(file_out_name),
                               file_out_fmt,
                               camera_number,frame_cntr,timestamp_us,video_output_format.c_str());

            FILE * file_out = fopen(file_out_name,"w");
            fwrite(iter.data,iter.size,1,file_out);
            fclose(file_out);
            printf("\nSaved frame %s\n",file_out_name);
        }

        if (timestamp_us != 0) //h264 / h264 can send meta data without timestamp (==0)
        {
            if (!first_frame_timestamp_us)
                first_frame_timestamp_us = timestamp_us;
            int64_t dt_us = timestamp_us - last_frame_timestamp_us;

            float fps = 1000000.0 / uint32_t(dt_us);
            if (average_fps == 0.0)
                average_fps = fps;
            average_fps = average_fps * 0.9 + fps * 0.1;
            char progress_char = '-';
            switch ((frame_cntr/2)%4){
                case 0: progress_char = '-'; break;
                case 1: progress_char = '\\'; break;
                case 2: progress_char = '|'; break;
                case 3: progress_char = '/'; break;
            }
            printf("%c [%d.%6d] fps %.2f, size %d\r",
                progress_char,(uint32_t)(timestamp_us/1000000),(uint32_t)(timestamp_us%1000000),average_fps,iter.size);
            //printf(".");
            fflush(stdout);
        }

        last_frame_timestamp_us = timestamp_us;

        // Write to MP4 file
        if ( save_video && ( (video_output_format=="h264") ||
                             (video_output_format=="h265") ) ) {
            mux->WriteBuffer(track, session, (iter), meta_data[buffer_index]);
        }

        // Send buffers to RTSP
        if (video_output_format=="h264"){
            RtspServerInterface::QuePushData("AVC",
                                            (uint8_t *) iter.data,
                                            iter.size,
                                            iter.timestamp,
                                            rtsp_video_queue);
        }
        else if (video_output_format=="h265"){
            RtspServerInterface::QuePushData("HEVC",
                                            (uint8_t *) iter.data,
                                            iter.size,
                                            iter.timestamp,
                                            rtsp_video_queue);
        }
    }

    auto ret = my_recorder.ReturnTrackBuffer(session, track, buffers);
    if (ret != NO_ERRORS) {
        printf("ReturnTrackBuffer failed!: %d\n", ret);
    }
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
void VideoTrackEventCb(uint32_t track_id, EventType event_type,
                       void *event_data, size_t event_data_size) {
    printf("Got a video track event callback\n");
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
int printGainAndExposure(int camera_number) {
    android::CameraMetadata meta;

    // Fetch the current camera parameters
    auto ret = my_recorder.GetCameraParam(camera_number, meta);
    if (ret != NO_ERRORS) {
        fprintf(stderr,
                "in printGainAndExposure: Unable to get camera parameters\n");
        return ERROR;
    }

    uint8_t aeMode           = meta.find(ANDROID_CONTROL_AE_MODE).data.u8[0];
    uint8_t awbMode          = meta.find(ANDROID_CONTROL_AWB_MODE).data.u8[0];
    int64_t exposure_time_ns = meta.find(ANDROID_SENSOR_EXPOSURE_TIME).data.i64[0];
    int32_t sensitivity      = meta.find(ANDROID_SENSOR_SENSITIVITY).data.i32[0];

    printf("AE Mode: %d, Exposure: %lldus, Gain: %dus, AWB Mode: %d\n",
           aeMode, exposure_time_ns/1000, sensitivity, awbMode);

    return NO_ERRORS;
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
int setGainAndExposure(int camera_number, int exposure_setting_us, int gain_setting, uint8_t awb_mode)
{
    android::CameraMetadata meta;

    uint8_t  ae_mode;
    int64_t  exposure_time_ns = (int64_t)(exposure_setting_us) * 1000;
    int32_t  sensitivity      = gain_setting;

    // Fetch the current camera parameters
    auto ret = my_recorder.GetCameraParam(camera_number, meta);
    if (ret != NO_ERRORS) {
        fprintf(stderr, "in setGainAndExposure: Unable to get camera parameters\n");
        return ERROR;
    }

    // update gain and exposure settings
    // if auto exposure is off, both exposure time and gain have to be set
    if ((exposure_setting_us == 0) || (gain_setting==0)) {
        ae_mode = ANDROID_CONTROL_AE_MODE_ON;
        meta.update(ANDROID_CONTROL_AE_MODE, &ae_mode, 1);             // copy 1 byte
    }
    else {
        ae_mode = ANDROID_CONTROL_AE_MODE_OFF;
        meta.update(ANDROID_CONTROL_AE_MODE,      &ae_mode, 1);        // copy 1 byte
    }

    meta.update(ANDROID_SENSOR_EXPOSURE_TIME, &exposure_time_ns, 1);   // copy 1 byte
    meta.update(ANDROID_SENSOR_SENSITIVITY,   &sensitivity, 1);        // copy 1 entry
    meta.update(ANDROID_CONTROL_AWB_MODE,     &awb_mode, 1);           // copy 1 byte

    // set new parameters
    ret = my_recorder.SetCameraParam(camera_number, meta);
    if (ret != NO_ERRORS) {
        fprintf(stderr, "Unable to update camera parameters: %d\n", ret);
        return ERROR;
    }

    // if successful, print out newly set parameters
    printf("Updated camera parameters\n");
    printGainAndExposure(camera_number);

    return NO_ERRORS;
}

int setExposure(int camera_number, int exposure_setting_us)
{
    android::CameraMetadata meta;

    // Fetch the current camera parameters
    auto ret = my_recorder.GetCameraParam(camera_number, meta);
    if (ret != NO_ERRORS) {
        fprintf(stderr, "in setExposure: Unable to get camera parameters\n");
        return ERROR;
    }

    uint8_t  aeMode           = ANDROID_CONTROL_AE_MODE_ON;
    if ( exposure_setting_us != 0 )
        aeMode                = ANDROID_CONTROL_AE_MODE_OFF;
    int64_t  exposure_time_ns = (int64_t)(exposure_setting_us) * 1000;
    meta.update(ANDROID_CONTROL_AE_MODE, &aeMode, 1);
    meta.update(ANDROID_SENSOR_EXPOSURE_TIME, &exposure_time_ns, 1);

    // set new parameters
    ret = my_recorder.SetCameraParam(camera_number, meta);
    if (ret != NO_ERRORS) {
        fprintf(stderr, "Unable to update camera parameters: %d\n", ret);
        return ERROR;
    }

    return 0;
}

int setGain(int camera_number, int gain_setting)
{
    android::CameraMetadata meta;

    // Fetch the current camera parameters
    auto ret = my_recorder.GetCameraParam(camera_number, meta);
    if (ret != NO_ERRORS) {
        fprintf(stderr, "in setExposure: Unable to get camera parameters\n");
        return ERROR;
    }

    uint8_t  aeMode           = ANDROID_CONTROL_AE_MODE_ON;
    if ( gain_setting != 0 )
        aeMode                = ANDROID_CONTROL_AE_MODE_OFF;
    int32_t  sensitivity      = gain_setting;
    meta.update(ANDROID_CONTROL_AE_MODE, &aeMode, 1);
    meta.update(ANDROID_SENSOR_SENSITIVITY, &sensitivity, 1);

    // set new parameters
    ret = my_recorder.SetCameraParam(camera_number, meta);
    if (ret != NO_ERRORS) {
        fprintf(stderr, "Unable to update camera parameters: %d\n", ret);
        return ERROR;
    }

    return 0;
}

int setBitrate(uint32_t track_number, uint32_t bitrate)
{
    uint32_t new_bitrate = bitrate;
    my_recorder.SetVideoTrackParam(session_id, track_number,
        qmmf::CodecParamType::kBitRateType, &new_bitrate, sizeof(new_bitrate));
}

int print_camera_specs(int camera_number)
{
    printf("Querying Camera Information...\n\n");
    android::CameraMetadata meta;

    // Fetch the current camera parameters
    auto ret = my_recorder.GetCameraParam(camera_number, meta);
    if (ret != NO_ERRORS) {
        fprintf(stderr, "in print_camera_specs: Unable to get camera parameters\n");
        return ERROR;
    }

    uint32_t width,height;
    camera_metadata_entry_t entry;

    //get raw sizes
    //ANDROID_SCALER_AVAILABLE_RAW_SIZES
    if (meta.exists(ANDROID_SCALER_AVAILABLE_RAW_SIZES)){
		entry = meta.find(ANDROID_SCALER_AVAILABLE_RAW_SIZES);
		printf("ANDROID_SCALER_AVAILABLE_RAW_SIZES:\n\t");
		for (uint32_t i = 0 ; i < entry.count; i += 2) {
			width = entry.data.i32[i+0];
			height = entry.data.i32[i+1];
			printf("%dx%d, ",width ,height);
		}
        printf("\n");
	}
    else{
		printf("%s:%d ANDROID_SCALER_AVAILABLE_RAW_SIZES does not exist",__func__,__LINE__);
	}
    printf("\n");

    //get video sizes
    if (meta.exists(ANDROID_SCALER_AVAILABLE_PROCESSED_SIZES)){
		entry = meta.find(ANDROID_SCALER_AVAILABLE_PROCESSED_SIZES);
		printf("ANDROID_SCALER_AVAILABLE_PROCESSED_SIZES:");
		for (uint32_t i = 0 ; i < entry.count; i += 2) {
            if (i%16==0)
                printf("\n\t");
			width = entry.data.i32[i+0];
			height = entry.data.i32[i+1];
			printf("%4dx%4d, ",width ,height);
		}
        printf("\n\n");
	}
    else{
		printf("%s:%d ANDROID_SCALER_AVAILABLE_PROCESSED_SIZES does not exist",__func__,__LINE__);
	}

    entry = meta.find(ANDROID_SENSOR_INFO_SENSITIVITY_RANGE);
	if (entry.count >= 2) {
        uint32_t min_sensitivity = entry.data.i32[0];
        uint32_t max_sensitivity = entry.data.i32[1];
		printf("ANDROID_SENSOR_INFO_SENSITIVITY_RANGE\n\tmin = %d\n\tmax = %d\n\n",min_sensitivity,max_sensitivity);
	}else{
		printf("ANDROID_SENSOR_INFO_SENSITIVITY_RANGE does not exist\n");
    }

    if (meta.exists(ANDROID_SENSOR_MAX_ANALOG_SENSITIVITY)){
	    entry = meta.find(ANDROID_SENSOR_MAX_ANALOG_SENSITIVITY);
	    printf("ANDROID_SENSOR_MAX_ANALOG_SENSITIVITY\n\t%d\n\n",entry.data.i32[0]);
    }
    else
    {
        printf("ANDROID_SENSOR_MAX_ANALOG_SENSITIVITY does not exist\n");
    }



    entry = meta.find(ANDROID_SENSOR_INFO_EXPOSURE_TIME_RANGE);
	if (entry.count >= 2) {
        uint64_t min_exposure = entry.data.i64[0];  //ns
        uint64_t max_exposure = entry.data.i64[1];  //ns
		printf("ANDROID_SENSOR_INFO_EXPOSURE_TIME_RANGE\n\tmin = %lldns\n\tmax = %lldns\n",min_exposure,max_exposure);
	}else{
		printf("ANDROID_SENSOR_INFO_EXPOSURE_TIME_RANGE does not exist\n");
    }

    printf("\n");
    return 0;
}


int try_parse_param(std::string buffer, std::string param_name, int * value_ptr)
{
    if ( buffer.compare(0,param_name.length(), param_name) != 0 )
    {
        return ERROR;
    }

    if ( buffer.length() < (param_name.length()+2) )
    {
        return ERROR;
    }

    int value = atoi(buffer.c_str()+param_name.length()+1);
    *value_ptr = value;

    return NO_ERRORS;
}

void read_control_input() {
    //note that setting gain or exposure to zero will result in enabling auto exposure/gain

    char read_buf[256];
    int num_read = read(control_fifo_fd,read_buf,sizeof(read_buf));
    if (num_read > 0)
    {
        //printf("%s\n",read_buf);
        std::string cmd(read_buf);
        int value = -1;
        if (try_parse_param(cmd,std::string("exposure_us"),&value) == NO_ERRORS)
        {
            printf("\nsetting exposure_us to: %d\n",value);
            setExposure(camera_number,value);
        }
        else if (try_parse_param(cmd,std::string("gain"),&value) == NO_ERRORS)
        {
            printf("\nsetting gain to: %d\n",value);
            setGain(camera_number,value);
        }
        else if (try_parse_param(cmd,std::string("bitrate"),&value) == NO_ERRORS)
        {
            printf("\nsetting bitrate to: %d\n",value);
            setBitrate(1,value);
        }
    }
}


//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
void help () {
    printf("Options:\n");
    printf("-i                Query and print camera information\n");
    printf("-w <mode>         Set white balance mode (off,auto,incandescent,fluorescent,warm-fluorescent,daylight,cloudy-daylight,twilight,shade)\n");
    printf("-e <exposure_us>  Set manual exposure (in microseconds). Make sure to set manual gain as well\n");
    printf("-g <gain>         Set manual camera gain (camera-specific). Make sure to set manual exposure as well\n");
    printf("-c <number>       Camera number (0 by default)\n");
    printf("-f <format>       Video output format selection (h264 (default), h265, jpeg, yuv, yuv-padded, raw8, raw10, raw12, blob)\n");
    printf("-s <resolution>   Image resolution (640x480 by default)\n");
    printf("-r <framerate>    Stream frame rate (30 by default)\n");
    printf("-b <bitrate>      Stream bit rate for h264/h265 (1M by default)\n");
    //printf("-q <quality>      Jpeg quality (0-100), default %d. Only if in '-f jpeg' mode\n",DEFAULT_JPEG_QUALITY);
    printf("-m <fifo_name>    Write raw data to a specified fifo (will be created)\n");
    printf("-o <filename>     Save video to filename (No save to file by default)\n");
    printf("-p <port>         Select port for RTSP server (%d by default)\n",DEFAULT_RTSP_PORT);
    printf("-a <angle>        Set rotation angle for the image (0 or 180 only)\n");
    printf("-t <time>         Session timeout in seconds\n");
    printf("-d <N>            Dump every N'th frame to disk. 0 to disable (default)\n");
    printf("-h                Show help\n");
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
int main (int argc, char **argv) {

    printf("voxl-rtsp version %s\n",APP_VERSION);

    int status = NO_ERRORS;

    SessionCb             session_cb;
    RecorderCb            recorder_status_cb;
    TrackCb               video_track_cb;
    VideoTrackCreateParam video_track_param;
    VideoExtraParam       extra_param;
    VideoRotate           video_rotate_plugin;
    qmmf_muxer_init_t     mux_params;  // Multiplex parameters for saving video to file

    // Start camera parameters
    struct CameraStartParam csp;
    csp.zsl_mode                = false; //disable zero shutter lag mode (not used)
    csp.enable_partial_metadata = false; //unused?
    csp.zsl_queue_depth         = 8;     //unused
    csp.zsl_width               = 3840;  //unused
    csp.zsl_height              = 2160;  //unused
    csp.frame_rate              = 30;    //unused? since frame rate is determined by track frame rate
    csp.flags                    = 0;

    bool print_camera_information = false;

    // Default video track parameters
    int track_width     = 640;
    int track_height    = 480;
    int track_framerate = 30;
    int track_bitrate   = 1000000;
    int jpeg_quality    = DEFAULT_JPEG_QUALITY;  //this setting currently has no effect

    // Auto exposure (AE) and Auto White Balance (ABW) settings
    uint8_t awb_mode          = ANDROID_CONTROL_AWB_MODE_AUTO;
    std::string awb_mode_str  = "auto";
    int  exposure_setting_us  = 0;  //0 = auto
    int  gain_setting         = 0;  //0 = auto

    int  rotation_angle       = 0;
    video_rotate_plugin.flags = RotationFlags::kNone;

    int rtsp_port        = DEFAULT_RTSP_PORT;
    int64_t timeout_sec  = 0;
    int64_t time_init_us = 0;
    int64_t time_from_start_us = 0;

    char *mp4_filename = NULL;

    bool output_fifo_enabled     = false;
    std::string output_fifo_name = std::string("/dev/frame0");

    // parse input args
    int opt;

    // put ':' in the starting of the
    // string so that program can
    //distinguish between '?' and ':'
    while ((opt = getopt(argc, argv, ":ia:d:w:e:g:c:s:r:b:f:m:o:p:t:h")) != -1) {
        switch (opt) {
        case 'a':
            rotation_angle = atoi(optarg);
            if (rotation_angle==0)
                video_rotate_plugin.flags = RotationFlags::kNone;
            else if (rotation_angle==180)
                video_rotate_plugin.flags = RotationFlags::kRotate180;
            else {
                fprintf(stderr, "Incorrect rotation angle %d. Only 0 or 180 are allowed\n",rotation_angle);
                return ERROR;
            }
            printf("Setting rotation angle to: %d\n",rotation_angle);
            break;

        case 'w':
            awb_mode_str = optarg;
            if (awb_mode_str=="off")
                awb_mode = ANDROID_CONTROL_AWB_MODE_OFF;
            else if (awb_mode_str=="auto")
                awb_mode = ANDROID_CONTROL_AWB_MODE_AUTO;
            else if (awb_mode_str=="incandescent")
                awb_mode = ANDROID_CONTROL_AWB_MODE_INCANDESCENT;
            else if (awb_mode_str=="fluorescent")
                awb_mode = ANDROID_CONTROL_AWB_MODE_FLUORESCENT;
            else if (awb_mode_str=="warm-fluorescent")
                awb_mode = ANDROID_CONTROL_AWB_MODE_WARM_FLUORESCENT;
            else if (awb_mode_str=="daylight")
                awb_mode = ANDROID_CONTROL_AWB_MODE_DAYLIGHT;
            else if (awb_mode_str=="cloudy-daylight")
                awb_mode = ANDROID_CONTROL_AWB_MODE_CLOUDY_DAYLIGHT;
            else if (awb_mode_str=="twilight")
                awb_mode = ANDROID_CONTROL_AWB_MODE_TWILIGHT;
            else if (awb_mode_str=="shade")
                awb_mode = ANDROID_CONTROL_AWB_MODE_SHADE;
            else{
                fprintf(stderr, "Unknown white balance mode: %s\n\n",awb_mode_str.c_str());
                help();
                return ERROR;
            }

            printf("Setting white balance mode to: %s\n",awb_mode_str.c_str());
            break;
        case 'e':
            exposure_setting_us = atoi(optarg);
            printf("Disabling auto exposure. Setting exposure to: %dus\n",exposure_setting_us);
            break;
        case 'g':
            gain_setting = atoi(optarg);
            printf("Disabling auto gain. Setting gain to: %d\n",gain_setting);
            break;
        case 'c':
            camera_number = atoi(optarg);
            printf("Setting camera number to: %d\n", camera_number);
            break;
        case 'd':
            dump_nth_frame = atoi(optarg);
            if (dump_nth_frame < 1)
                dump_nth_frame = 0;

            printf("Setting dump n'th frame to: %d\n",dump_nth_frame);
            break;
        case 's':
        {
            char* w = strtok(optarg, "x");
            char* h = strtok(NULL, "x");
            if(w == NULL || h == NULL) {
                fprintf(stderr, "Error - incorrect resolution format: WIDTHxHEIGHT\n\n");
                help();
                return ERROR;
            } else {
                track_width = atoi(w);
                track_height = atoi(h);
                printf("Setting resolution: %dx%d\n", track_width, track_height);
            }
            break;
        }
        case 'r':
            track_framerate = atoi(optarg);
            printf("Setting frame rate (default 30 fps): %d\n", track_framerate);
            break;
        case 'b':
            track_bitrate = atoi(optarg);
            printf("Setting bit rate (default 1M): %d\n", track_bitrate);
            break;
        case 'f':
            video_output_format = std::string(optarg);
            if ( !(video_output_format=="h264") && !(video_output_format=="h265")       && !(video_output_format=="jpeg") &&
                 !(video_output_format=="yuv")  && !(video_output_format=="yuv-padded") && !(video_output_format=="blob") &&
                 !(video_output_format=="raw8") && !(video_output_format=="raw10")      && !(video_output_format=="raw12") ) {
                fprintf(stderr, "Error - incorrect video output format (h264,h265,jpeg,yuv,yuv-padded,blob,raw8,raw10,raw12)\n\n");
                help();
                return ERROR;
            }
            printf("Setting output format to: %s\n",video_output_format.c_str());
            break;
        case 'm':
            output_fifo_enabled = true;
            output_fifo_name    = std::string(optarg);
            printf("Enabling output fifo %s\n",output_fifo_name.c_str());
            break;
        case 'o':
            mp4_filename = optarg;
            if (std::string(mp4_filename)=="stdout")
                write_to_stdout = 1;
            else {
                save_video = true;
                printf("Setting MP4 filename: %s\n", mp4_filename);
            }
            break;
        case 'i':
            print_camera_information = true;
            break;
        case 'h':
            help();
            return ERROR;
        case 'p':
            rtsp_port = atoi(optarg);
            printf("Setting RTSP port to: %d\n",rtsp_port);
            break;
        case 't':
            timeout_sec = atoi(optarg);
            if (timeout_sec<0) timeout_sec = 0;
            printf("Setting timeout to: %lld\n",timeout_sec);
            break;
        case 'q':
            //WARNING: this setting is currently not working and is disabled
            jpeg_quality = atoi(optarg);
            if ((jpeg_quality<1) || (jpeg_quality>100))
            {
                fprintf(stderr, "Error - jpeg quality must be between 0 and 100 (%d provided)\n",jpeg_quality);
                return ERROR;
            }
            printf("Setting jpeg quality to: %d\n",jpeg_quality);
            break;
        case ':':
            fprintf(stderr, "Error - option needs a value\n\n");
            help();
            return ERROR;
        case '?':
            fprintf(stderr, "Error - unknown option: %c\n\n", optopt);
            help();
            return ERROR;
        }
    }

    // optind is for the extra arguments
    // which are not parsed
    for (; optind < argc; optind++) {
        fprintf(stderr, "extra arguments: %s\n", argv[optind]);
        help();
        return ERROR;
    }

    // RTSP parameters
    RtspServerInterface* rtsp_server = NULL;
    size_t url_size;
    char* rtsp_url;

    // Connect
    auto ret = my_recorder.Connect(recorder_status_cb);
    recorder_status_cb.event_cb = [&] (EventType event_type, void *event_data,
                                       size_t event_data_size)
                                      { RecorderEventCb(event_type, event_data,
                                                        event_data_size); };
    if (ret != NO_ERRORS) {
        fprintf(stderr, "Unable to connect to recorder service: %d\n", ret);
        return ERROR;
    }
    else {
        printf("Connected\n");
    }

    // Start camera
    ret = my_recorder.StartCamera(camera_number, csp);
    if (ret != NO_ERRORS) {
        fprintf(stderr, "Unable to start camera: %d\n", ret);
        status = ERROR;
        goto disconnect;
    }
    else {
        printf("Started camera %d\n", camera_number);
    }

    // Create session
    session_cb.event_cb = [&] (EventType event_type, void *event_data,
                               size_t event_data_size)
                              { SessionEventCb(event_type, event_data,
                                               event_data_size); };
    ret = my_recorder.CreateSession(session_cb, &session_id);
    if (ret != NO_ERRORS) {
        fprintf(stderr, "Unable to connect to create session: %d\n", ret);
        status = ERROR;
        goto stopcamera;
    }
    else {
        printf("Created session\n");
    }

    // Create video track
    memset(&video_track_param, 0x0, sizeof video_track_param);

    // Video track parameters
    video_track_param.camera_id      = camera_number;
    video_track_param.width          = track_width;
    video_track_param.height         = track_height;
    video_track_param.frame_rate     = track_framerate;
    video_track_param.low_power_mode = true;

    //turn off low power mode if using high frame rates
    //actually 60fps still works in low power mode
    //it is not clear whether any power savings exist in low power mode
    //but there could be additional delays in camera pipeline (more efficient?)
    if (track_framerate > 30){
        video_track_param.low_power_mode = false;
    }

    //h264 codec params
    if (video_output_format=="h264")
    {
        video_track_param.format_type = VideoFormat::kAVC;
        video_track_param.codec_param.avc.idr_interval = 1;
        video_track_param.codec_param.avc.bitrate  = track_bitrate;
        video_track_param.codec_param.avc.profile = AVCProfileType::kHigh;
        video_track_param.codec_param.avc.level = AVCLevelType::kLevel3;
        video_track_param.codec_param.avc.ratecontrol_type =
            VideoRateControlType::kMaxBitrate;
        video_track_param.codec_param.avc.qp_params.enable_init_qp = true;
        video_track_param.codec_param.avc.qp_params.init_qp.init_IQP = 27;
        video_track_param.codec_param.avc.qp_params.init_qp.init_PQP = 28;
        video_track_param.codec_param.avc.qp_params.init_qp.init_BQP = 28;
        video_track_param.codec_param.avc.qp_params.init_qp.init_QP_mode = 0x7;
        video_track_param.codec_param.avc.qp_params.enable_qp_range = true;
        video_track_param.codec_param.avc.qp_params.qp_range.min_QP = 10;
        video_track_param.codec_param.avc.qp_params.qp_range.max_QP = 51;
        video_track_param.codec_param.avc.qp_params.enable_qp_IBP_range = true;
        video_track_param.codec_param.avc.qp_params.qp_IBP_range.min_IQP = 10;
        video_track_param.codec_param.avc.qp_params.qp_IBP_range.max_IQP = 51;
        video_track_param.codec_param.avc.qp_params.qp_IBP_range.min_PQP = 10;
        video_track_param.codec_param.avc.qp_params.qp_IBP_range.max_PQP = 51;
        video_track_param.codec_param.avc.qp_params.qp_IBP_range.min_BQP = 10;
        video_track_param.codec_param.avc.qp_params.qp_IBP_range.max_BQP = 51;
    }

    //h265 codec params
    else if (video_output_format=="h265")
    {
        video_track_param.format_type = VideoFormat::kHEVC;
        video_track_param.codec_param.hevc.idr_interval = 1;
        video_track_param.codec_param.hevc.bitrate = track_bitrate;
        video_track_param.codec_param.hevc.profile = HEVCProfileType::kMain;
        video_track_param.codec_param.hevc.level = HEVCLevelType::kLevel5_1;
        video_track_param.codec_param.hevc.ratecontrol_type = VideoRateControlType::kMaxBitrate;
        video_track_param.codec_param.hevc.qp_params.enable_init_qp = true;
        video_track_param.codec_param.hevc.qp_params.init_qp.init_IQP = 27;
        video_track_param.codec_param.hevc.qp_params.init_qp.init_PQP = 28;
        video_track_param.codec_param.hevc.qp_params.init_qp.init_BQP = 28;
        video_track_param.codec_param.hevc.qp_params.init_qp.init_QP_mode = 0x7;
        video_track_param.codec_param.hevc.qp_params.enable_qp_range = true;
        video_track_param.codec_param.hevc.qp_params.qp_range.min_QP = 10;
        video_track_param.codec_param.hevc.qp_params.qp_range.max_QP = 51;
        video_track_param.codec_param.hevc.qp_params.enable_qp_IBP_range = true;
        video_track_param.codec_param.hevc.qp_params.qp_IBP_range.min_IQP = 10;
        video_track_param.codec_param.hevc.qp_params.qp_IBP_range.max_IQP = 51;
        video_track_param.codec_param.hevc.qp_params.qp_IBP_range.min_PQP = 10;
        video_track_param.codec_param.hevc.qp_params.qp_IBP_range.max_PQP = 51;
        video_track_param.codec_param.hevc.qp_params.qp_IBP_range.min_BQP = 10;
        video_track_param.codec_param.hevc.qp_params.qp_IBP_range.max_BQP = 51;
        video_track_param.codec_param.hevc.ltr_count = 0;
        video_track_param.codec_param.hevc.insert_aud_delimiter = false;
        video_track_param.codec_param.hevc.hier_layer = 0;
        video_track_param.codec_param.hevc.prepend_sps_pps_to_idr = false;
        video_track_param.codec_param.hevc.sar_enabled = false;
        video_track_param.codec_param.hevc.sar_width = 0;
        video_track_param.codec_param.hevc.sar_height = 0;
    }
    else if ((video_output_format=="yuv") || (video_output_format=="yuv-padded"))
    {
        video_track_param.format_type   = VideoFormat::kYUV;
    }
    else if (video_output_format=="raw8")
    {
        video_track_param.format_type   = VideoFormat::kBayerRDI8BIT;
    }
    else if (video_output_format=="raw10")
    {
        video_track_param.format_type   = VideoFormat::kBayerRDI10BIT;
    }
    else if (video_output_format=="raw12")
    {
        video_track_param.format_type   = VideoFormat::kBayerRDI12BIT;
    }
    else if (video_output_format=="blob")
    {
        video_track_param.format_type   = VideoFormat::kBlob;
    }
    else if (video_output_format=="jpeg")
    {
        video_track_param.format_type                        = VideoFormat::kJPEG;
        video_track_param.codec_param.jpeg.enable_thumbnail  = false;
        video_track_param.codec_param.jpeg.quality           = jpeg_quality; //not working??
        video_track_param.codec_param.jpeg.thumbnail_quality = 0;   //not used
        video_track_param.codec_param.jpeg.thumbnail_height  = 240; //not used
        video_track_param.codec_param.jpeg.thumbnail_width   = 320; //not used
    }
    else
    {
        printf("ERROR: unknown track format: %s",video_output_format.c_str());
        status = ERROR;
        goto stopsession;
    }


    video_track_cb.data_cb = [&] (uint32_t track_id,
                                  std::vector<BufferDescriptor> buffers,
                                  std::vector<MetaData> meta_data)
                                 { VideoTrackDataCb(session_id, track_id,
                                                    buffers, meta_data); };
    video_track_cb.event_cb = [&] (uint32_t track_id,
                                   EventType event_type,
                                   void *event_data,
                                   size_t event_data_size)
                                  { VideoTrackEventCb(track_id, event_type,
                                                      event_data,
                                                      event_data_size); };

    extra_param.Update(QMMF_VIDEO_ROTATE, video_rotate_plugin);

    //ret = my_recorder.CreateVideoTrack(session_id, 1, video_track_param, video_track_cb);
    ret = my_recorder.CreateVideoTrack(session_id, 1, video_track_param, extra_param, video_track_cb);
    if (ret != NO_ERRORS) {
        fprintf(stderr, "Unable to connect to create video track: %d\n", ret);
        status = ERROR;
        goto deletesession;
    }
    else {
        printf("Video track created\n");
    }

    if (print_camera_information){
        print_camera_specs(camera_number);
        goto stopsession;
    }


    setGainAndExposure(camera_number, exposure_setting_us, gain_setting, awb_mode);

    // Start RTSP server
    if ( (video_output_format=="h264") || (video_output_format=="h265") )
    {
        rtsp_server = new RtspServerInterface(rtsp_port);
        if (rtsp_server == nullptr) {
            fprintf(stderr, "Could not create RTSP server\n");
            status = ERROR;
            goto stopsession;
        }
        rtsp_server->CreateSMS();
        rtsp_video_queue = nullptr;
        RtspServerInterface::QueueInit(&rtsp_video_queue);

        if (video_output_format=="h264")
        {
            mux_params.video_stream.format = MUX_STREAM_VIDEO_H264;
            rtsp_server->AddVideoSMSSToSMS(VIDEO_FORMAT_H264,
                                        track_framerate,
                                        rtsp_video_queue);
        }
        if (video_output_format=="h265")
        {
            mux_params.video_stream.format = MUX_STREAM_VIDEO_H265;
            rtsp_server->AddVideoSMSSToSMS(VIDEO_FORMAT_H265,
                                        track_framerate,
                                        rtsp_video_queue);
        }

        rtsp_server->StartTaskScheduler();

        url_size = rtsp_server->GetURLSize();
        if (url_size) {
            rtsp_url = (char *) malloc(url_size);
            if (rtsp_url) {
                rtsp_server->GetURL(rtsp_url, url_size);
                printf("%s\n", rtsp_url);
            }
            else {
                fprintf(stderr, "No resources for URL string!\n");
                status = ERROR;
                goto deletetracks;
            }
        }
        else {
            fprintf(stderr, "RTSP URL size is invalid!\n");
            status = ERROR;
            goto deletetracks;
        }
    }


    // Setup the multiplex options if we are creating an MP4 file
    if (save_video) {
        int32_t mux_init_return = NO_ERRORS;

        mux = new MuxInterface(MUX_BRAND_MP4, mp4_filename);
        if (!mux){
            fprintf(stderr, "ERROR: could not create mux interface\n");
            status = ERROR;
            goto deletetracks;
        }

        // Setup the parameters for the MP4 file
        mux_params.audio_stream_present       = false;
        mux_params.video_stream_present       = true;
        mux_params.video_stream.codec_profile = 100;
        mux_params.video_stream.codec_level   = 30;
        mux_params.video_stream.format        = MUX_STREAM_VIDEO_H264;
        mux_params.video_stream.width         = video_track_param.width;
        mux_params.video_stream.height        = video_track_param.height;
        mux_params.video_stream.bitrate       = video_track_param.codec_param.avc.bitrate;
        mux_params.video_stream.framerate     = video_track_param.frame_rate;
        mux_params.video_stream.track_id      = 1;
        mux_params.brand                      = MUX_BRAND_MP4;
        mux_params.release_cb = [&] (uint32_t track_id, uint32_t session_id,
                                    BufferDescriptor &buffer) {};
        mux_init_return = mux->Init(mux_params);
        if (mux_init_return != NO_ERRORS) {
            fprintf(stderr, "ERROR: multiplexer init failed: %d", mux_init_return);
            mux_init_return = ERROR;
            status = ERROR;
            goto deletetracks;
        }
    }

    if (output_fifo_enabled){
        create_output_fifo(output_fifo_name);
    }

    create_control_fifo();

    // Start session
    ret = my_recorder.StartSession(session_id);
    if (ret != NO_ERRORS) {
        fprintf(stderr, "Unable to connect to start session: %d\n", ret);
        status = ERROR;
        goto deletetracks;
    }
    else {
        printf("Started session\n");
    }

    // Run until commanded to shutdown.
    // Note: If the process is ended by "kill -9" or if
    // power is cut then if an mp4 was being saved it will
    // not be complete and, hence, unreadable.
    printf("Ctrl-c or 'kill %d' to end\n", getpid());
    signal(SIGHUP, signal_handler);
    signal(SIGINT, signal_handler);
    signal(SIGTERM, signal_handler);

    time_init_us = get_time_us();
    while (running){
        int64_t time_now_us = get_time_us();
        time_from_start_us = time_now_us - time_init_us;
        if ( (timeout_sec>0) && ( time_from_start_us > (timeout_sec * 1000000) ) ) {
            running = false;
            printf("Session time expired (%f sec).. Exiting \n",(float)(time_from_start_us*0.000001));
        }
        read_control_input();
        usleep(100000);
    }

    // Stop session
stopsession:
    printf("\n");
    ret = my_recorder.StopSession(session_id, true);
    if (ret != NO_ERRORS) {
        fprintf(stderr, "Unable to connect to stop session: %d\n", ret);
        return ERROR;
    }
    else {
        printf("Stopped session\n");
    }

    if (rtsp_server != NULL) {
        rtsp_server->StopTaskScheduler();
        rtsp_server->ResetRtspServer();
        RtspServerInterface::QueueDInit(&rtsp_video_queue);
        delete rtsp_server;
    }

    // Delete video track
deletetracks:
    ret = my_recorder.DeleteVideoTrack(session_id, 1);
    if (ret != NO_ERRORS) {
        fprintf(stderr, "Unable to delete video track: %d\n", ret);
        return ERROR;
    }
    else {
        printf("Video track deleted\n");
    }

deletesession:
    ret = my_recorder.DeleteSession(session_id);
    if (ret != NO_ERRORS) {
        fprintf(stderr, "Unable to delete session: %d\n", ret);
        return ERROR;
    }
    else {
        printf("Session deleted\n");
    }

stopcamera:
    ret = my_recorder.StopCamera(camera_number);
    if (ret != NO_ERRORS) {
        fprintf(stderr, "Unable to connect to stop camera: %d\n", ret);
        return ERROR;
    }
    else {
        printf("Stopped Camera\n");
    }

disconnect:
    ret = my_recorder.Disconnect();
    if (ret != NO_ERRORS) {
        fprintf(stderr, "Unable to disconnect from recorder service: %d\n", ret);
        return ERROR;
    }
    else
    {
        printf("Disconnected\n");
    }

    // Gracefully stop the mp4 file multiplexer
    if (save_video) {
        if (mux){
            mux->Stop();
            delete mux;
            mux = NULL;
        }
    }

    //close the output fifo if needed
    if (output_fifo_enabled){
        if (output_fifo_fd>=0)
        {
            close(output_fifo_fd);
            output_fifo_fd = -1;
            printf("closed output fifo\n");
        }
    }

    if (control_fifo_fd>=0){
        close(control_fifo_fd);
        control_fifo_fd= -1;
    }

    //don't print stats if we are just querying camera info
    if (print_camera_information)
        return 0;

    float total_duration  = (float)(time_from_start_us*0.000001);
    float bandwidth_mbps  = 0;
    float total_mbytes    = track_bytes_total/(1024.0*1024.0);
    float bytes_per_frame = 0;

    if (track_bytes_total && time_from_start_us)
        bandwidth_mbps  = track_bytes_total/total_duration * 8.0 / (1024.0*1024.0);

    if (track_bytes_total && frame_cntr)
        bytes_per_frame = track_bytes_total / frame_cntr;

    printf("voxl-rtsp exiting\n");
    printf("\n");
    printf("Summary:\n");
    printf("Camera #         : %d\n", camera_number);
    printf("Resolution       : %dx%d\n", track_width, track_height);
    printf("Format           : %s\n",video_output_format.c_str());
    printf("FPS Desired      : %.2f\n",(float)track_framerate);
    printf("FPS Actual       : %.2f\n",average_fps);
    printf("Duration (s)     : %.3f\n",total_duration);
    printf("Frame Count      : %d\n",frame_cntr);
    printf("Total Size (MB)  : %.2f\n",total_mbytes);
    printf("Bandwidth (Mbps) : %.2f\n",bandwidth_mbps);
    printf("Bytes per Frame  : %d\n",(uint32_t)bytes_per_frame);

    return status;
}
